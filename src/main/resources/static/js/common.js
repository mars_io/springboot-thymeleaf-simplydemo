jQuery.Mars = {
    login : function (email, pass) {

        $.ajax(
            {
                type : "post",
                url : "/auth/ajax",
                data: {
                    email : email,
                    pass : pass
                },
                success : function(data){
                    try {
                        if (data.status == 2000){
                            window.location.href = "/books/";
                        } else {
                            alert(data.message);
                        }
                    } catch (e) {
                        return false;
                    }
                },
                error : function(xhr) {
                    console.log(xhr.status)
                }

            }
        );
    }
};


$(function () {

    $("#login_button").click(function () {
        $.Mars.login(
            $("input[name='email']").val(),
            $("input[name='pass']").val()
        );
    });
});