package com.mars.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

/**
 * Created by Mars on 2019/5/18 0018
 */

//TODO: Session HandlerInterceptor

@Controller
public class BookController {

    @Autowired
    private HttpSession httpSession = null;

    @Autowired
    private ServletContext servletContext = null;

    private String pageTitle = "Books list";

    @RequestMapping("/books/")
    public String index() {

        if (httpSession.getAttribute("user") == null) {

            return "redirect:/login";
        }

        servletContext.setAttribute("pageTitle", pageTitle);
        System.out.println(httpSession.getAttribute("user"));
        return "index";
    }


}
