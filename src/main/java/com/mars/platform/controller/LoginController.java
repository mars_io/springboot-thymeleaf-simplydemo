package com.mars.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mars on 2019/5/18 0018
 */

@Controller
public class LoginController {

    private final String email = "test_email@gmail.com";

    private final String pass = "test_pass";

    private String pageTitle = "Login form";

    @Autowired
    private ServletContext servletContext = null;

    @Autowired
    private HttpSession httpSession = null;

    @Autowired
    private ServletRequest servletRequest = null;


    @GetMapping("/login")
    public String login() {

        servletContext.setAttribute("pageTitle", pageTitle);
        return "login";
    }

    @PostMapping("/auth/form")
    public String authForm(@RequestParam("email") String email,
                           @RequestParam("pass") String pass) {

        if (email.equals(this.email) && pass.equals(this.pass)) {

            httpSession.setAttribute("user", this.email);
            httpSession.setAttribute("pass", this.pass);
        } else {

            return "redirect:/error";
        }
        System.out.println("login" + this.email + this.pass);
        return "redirect:/books/";
    }

    @PostMapping("/auth/ajax")
    @ResponseBody
    public Map authAjax(Map result,
                        @RequestParam("email") @NotEmpty @Email String email,
                        @RequestParam("pass") @NotEmpty String pass) {

        String testStr = (String) servletRequest.getAttribute("ttt");
        System.out.println(testStr);
        result = new HashMap();
        if (email.isEmpty() || pass.isEmpty()) {
            result.put("status", 2001);
            result.put("message", "email or pass can't be empty.");

        } else {
            if (email.equals(this.email) && pass.equals(this.pass)) {
                httpSession.setAttribute("user", email);
                httpSession.setAttribute("pass", pass);
                result.put("status", 2000);
                result.put("message", "auth success");
            } else {
                result.put("status", 2002);
                result.put("message", "Wrong email or pass.");
            }
        }

        return result;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout() {
        httpSession.invalidate();
        return "redirect:/login";
    }

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public String error() {

        return "error";
    }
}
